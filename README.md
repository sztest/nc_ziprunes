*A mid/high-cost fast-travel system for NodeCore that allows
non-gameplay-breaking inventory transport.*

Players can construct networks of relativistic-speed travel paths
using charcoal glyphs, powered at the starting points by lux.  Players
standing at the starting point of a path will be zipped along that
path at the maximum speed allowed by map loading.

* Paths are constructed of a series of segments.
* Each segment consists of 2 V runes in line, with one pointing at
  the other, and the other pointing in the same direction.
* There must be no obstacles along the path that would block a
  player moving along it
* There does not need to be a floor under the path.
* Path segments can be in any of the 6 directions.
* A path can be made of an unlimited number of segments in any
  mix of directions.
* A path segment can only be a limited maximum length, by default
  up to 32 nodes long.
* When traveling along a path, the player will land at the position
  one node past the end of the destination rune.
* For upward paths, the player will also be shifted one node to
  stand on top of the node that the destination rune was drawn
  on.
* To travel along a path, a player needs to be "energized" by
  standing still on a starting glyph.
* A starting glyph needs to be drawn on a piece of lux cobble with
  at least a certain minimum energy (by default 4+ neighbors) or
  on a node with flux on the opposite side of the glyph.
* The player will continue to travel along any path with a staring
  position within 1 node of the player, so long as the player
  remains "energized"
* The player will remain "energized" until moving by moving other
  than by traveling along paths, e.g. walking, falling, etc.
* All areas traveled must be loaded by the server, so this may
  limit speed.

Paths of unlimited length are relatively inexpensive to construct,
requiring by default only 5 lux cobble, a supply of coal lumps,
and the patience and care to plan and construct.

Unlike with other cheap fast travel mods intended for touring or
visiting, ZipRunes allow players to carry their items with them.